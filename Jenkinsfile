// 流水线脚本

pipeline {
    // CICD 流程都在这里定义

    //如果在集群模式下，任意一台机器都可以作为代理执行
    agent any

    //定义流水线的环境信息
    environment {
        WS = "${WORKSPACE}"
        ALIYUN_SECRTE = credentials("aliyun-docker-repo")
        IMAGE_VRESION = "v1.0"
    }

    //定义流水线的阶段
    stages {
        //环境检查
        stage('---环境检查---') {
            steps {
               //双引号单引号都可以，建议进行环境变量获取的时候使用双引号
               echo '---环境检查---'
               sh 'printenv'
               sh 'java --version'
               sh 'git --version'
               sh 'docker version'
               sh 'pwd && ls -alh'
            }
        }
        //编译
        stage('---编译---') {
            agent {
                //这条命令会在宿主机中启动一个容器，但是使用的文件还是Jenkins容器中的，
                //还有Jenkins容器的工作目录在宿主机上进行了挂载，因此在使用 maven 的时候
                //将 maven 的仓库和配置文件存放在共享目录中，这样Jenkins既可以使用宿主机中
                //的命令，又可以使用本容器中的配置文件，做出的修改还可以在宿主机中感知到。
                docker { image 'maven:3-alpine' }

            }
            steps {
               //编译阶段具体要做的步骤
               echo "---编译---"
               //打包为 .jar，指定自定义的 settings.xml，使用 阿里云 加速
               sh 'cd ${WS} && mvn clean package -s /var/jenkins_home/appconfig/maven/settings.xml  -Dmaven.test.skip=true'
               //jar 包推送给 maven repository
            }
        }
        //测试
        stage('测试') {
            steps {
                //测试阶段具体要做的步骤
                echo "---测试---"
            }
        }
        //生成镜像
        stage('生成镜像') {
            steps {
                //打包阶段具体要做的步骤
                echo "---打包---"
                //检查 jenkins 是否能运行 docker 命令
                sh 'docker build -t jenkins-demo .'
            }
        }
        //推送镜像
        stage('推送镜像') {
            input {
                message "是否推送镜像？"
                ok "是"
                submitter "alice,bob"
                parameters {
                    string(name: 'IMAGE_VRESION', defaultValue: 'v1.0', description: '推送的镜像版本')
                }
            }
            steps {
                //将镜像推送到阿里欲奴
                echo "---推送镜像---"
                //镜像保存推送
                sh 'docker login -u ${ALIYUN_SECRTE_USR} -p ${ALIYUN_SECRTE_PSW} registry.cn-beijing.aliyuncs.com'
                sh 'docker tag jenkins-demo registry.cn-beijing.aliyuncs.com/l_fyyyy/jenkins-demo:${IMAGE_VRESION}'
                sh 'docker push registry.cn-beijing.aliyuncs.com/l_fyyyy/jenkins-demo:${IMAGE_VRESION}'

            }
        }
        //部署
        stage('部署') {
            //选择部署的区域
            input {
              message '请选择部署位置'
              parameters {
                choice choices: ['bj-1', 'sh-2', 'sz-3', 'hz-1'], 
                description: '请选择部署的区域', 
                name: 'DEPLOY_PLACE'
              }
            }

            //以后要实现的功能：
            //1. 根据提交的内容识别是哪个分区，如果是 master 分支则部署到生产环境中，
            //如果是 dev 分支则部署到测试环境
            //2. 识别提交的是哪个微服务，根据不同的微服务部署到不同的机器。
            //（因为在钩子函数回调会携带分支信息以及服务名字信息）
            steps {
                //根据部署的区域进行部署
                script {
                    def where = "${DEPLOY_PLACE}"
                    if (where == "bj-1") {
                        sh "echo 已部署到北京一区"
                    } else if (where == "sh-2") {
                        sh "echo 已部署到上海二区"
                    } else if (where == "sz-3") {
                        sh "echo 已部署到深圳三区"
                    } else {
                        sh "echo 已部署到杭州一区"
                    }
                }

                //部署阶段具体要做的步骤
                echo "---部署---"
                sh 'docker rm -f jenkins-demo-dev'
                sh 'docker run -d -p 8888:8080  --name jenkins-demo-dev jenkins-demo'
            }
        }
        //发送报告
        stage('发送报告') {
            steps {
                emailext body: '''<!DOCTYPE html>
                <html>
                <head>
                <meta charset="UTF-8">
                <title>${ENV, var="JOB_NAME"}-第${BUILD_NUMBER}次构建日志</title>
                </head>

                <body leftmargin="8" marginwidth="0" topmargin="8" marginheight="4" offset="0">
                    <table width="95%" cellpadding="0" cellspacing="0" style="font-size: 11pt; font-family: Tahoma, Arial, Helvetica, sans-serif">
                        <tr>
                            <td>(本邮件由程序自动下发，请勿回复！)</td>
                        </tr>
                        <tr>
                            <td>
                                <h2><font color="#FF0000">构建结果 - ${BUILD_STATUS}</font></h2>
                            </td>
                        </tr>
                        <tr>
                            <td><br />
                                <b><font color="#0B610B">构建信息</font></b>
                                <hr size="2" width="100%" align="center" />
                            </td>
                        </tr>
                        <tr>   SVN_URL
                            <td>
                                <ul>
                                    <li>项目名称：${PROJECT_NAME}</li>
                                    <li>SVN路径：${SVN_URL}</li>                    
                                    <li>构建编号：${BUILD_NUMBER}</li>                    
                                    <li>SVN版本：${SVN_REVISION}</li>
                                    <li>触发原因：${CAUSE}</li>   
                                    <li>构建日志：<a href="${BUILD_URL}console">${BUILD_URL}console</a></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><font color="#0B610B">变更信息:</font></b>
                               <hr size="2" width="100%" align="center" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ul>
                                    <li>上次构建成功后变化 :  ${CHANGES_SINCE_LAST_SUCCESS}</a></li>
                                </ul>    
                            </td>
                        </tr>
                 <tr>
                            <td>
                                <ul>
                                    <li>上次构建不稳定后变化 :  ${CHANGES_SINCE_LAST_UNSTABLE}</a></li>
                                </ul>    
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ul>
                                    <li>历史变更记录 : <a href="${PROJECT_URL}changes">${PROJECT_URL}changes</a></li>
                                </ul>    
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ul>
                                    <li>变更集:${JELLY_SCRIPT,template="html"}</a></li>
                                </ul>    
                            </td>
                        </tr>
                        <!--
                        <tr>
                            <td>
                                <b><font color="#0B610B">Failed Test Results</font></b>
                                <hr size="2" width="100%" align="center" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <pre style="font-size: 11pt; font-family: Tahoma, Arial, Helvetica, sans-serif">$FAILED_TESTS</pre>
                                <br />
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <b><font color="#0B610B">构建日志 (最后 100行):</font></b>
                                <hr size="2" width="100%" align="center" />
                            </td>
                        </tr>-->
                        <!-- <tr>
                            <td>Test Logs (if test has ran): <a
                                href="${PROJECT_URL}ws/TestResult/archive_logs/Log-Build-${BUILD_NUMBER}.zip">${PROJECT_URL}/ws/TestResult/archive_logs/Log-Build-${BUILD_NUMBER}.zip</a>
                                <br />
                            <br />
                            </td>
                        </tr> -->
                        <!--
                        <tr>
                            <td>
                                <textarea cols="80" rows="30" readonly="readonly" style="font-family: Courier New">${BUILD_LOG, maxLines=100,escapeHtml=true}</textarea>
                            </td>
                        </tr>-->
                        <hr size="2" width="100%" align="center" />
                 
                    </table>

                </body>
                </html>''', subject: '${ENV, var="JOB_NAME"}-第${BUILD_NUMBER}次构建日志', to: 'l_fyyyy@163.com'
            }
        }
        // stage('发布版本') {
        //     //1.手动输入版本
        //     echo '发布版本'
        //     //2.代码镜像的保存
        // }
    }

}